import { Component, OnInit } from '@angular/core';
import { VolunteerTypeService } from 'src/app/services/volunteer-type.service';
import { VolunteerType } from 'src/app/models/volunteer-type';

@Component({
  selector: 'app-event-register',
  templateUrl: './event-register.component.html',
  styleUrls: ['./event-register.component.css']
})
export class EventRegisterComponent implements OnInit {
  constructor(private VolunteerTypeService : VolunteerTypeService) { }

  types: VolunteerType[];
  selectedValue: string;

  ngOnInit(): void {
    this.VolunteerTypeService.getVolunteerTypes().subscribe(data => this.types = data);
  }

}
