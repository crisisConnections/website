import { Component, OnInit, Input } from '@angular/core';
import { VolunteerEvent } from 'src/app/models/volunteer-event';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RegisterEventDialogComponent } from '../register-event-dialog/register-event-dialog.component';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  @Input() eventToShow : VolunteerEvent;
  
  constructor(public dialog: MatDialog) { }

  openDialog() {
    const dialogRef = this.dialog.open(RegisterEventDialogComponent, {
      data: this.eventToShow
    });
  }

  ngOnInit(): void {
  }

}
