import { Component, OnInit } from '@angular/core';
import { VolunteerEventService } from 'src/app/services/volunteer-event.service';
import { VolunteerEvent } from 'src/app/models/volunteer-event';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.css']
})
export class EventsListComponent implements OnInit {

  constructor(private volunteerEventService : VolunteerEventService) { }

  events : VolunteerEvent[];

  ngOnInit(): void {
    this.volunteerEventService.getEvents().subscribe(data => this.events = data);
  }

}
