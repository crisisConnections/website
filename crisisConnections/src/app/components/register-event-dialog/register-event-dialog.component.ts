import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { VolunteerEvent } from 'src/app/models/volunteer-event';

@Component({
  selector: 'app-register-event-dialog',
  templateUrl: './register-event-dialog.component.html',
  styleUrls: ['./register-event-dialog.component.css']
})
export class RegisterEventDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: VolunteerEvent) { }

  ngOnInit(): void {
  }

}
