import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventRegisterComponent } from './components/event-register/event-register.component';
import { MainPageComponent } from './components/main-page/main-page.component';


const routes: Routes = [
  { path: 'main-page', component: MainPageComponent},
  { path: 'event-register', component: EventRegisterComponent},
  { path: '',   redirectTo: '\main-page', pathMatch: 'full' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }