import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { VolunteerEvent } from '../models/volunteer-event';
import { Address } from '../models/address';
import { User } from '../models/user';
import { VolunteerType } from '../models/volunteer-type';

@Injectable({
  providedIn: 'root'
})
export class VolunteerEventService {

  getEvents() : Observable<VolunteerEvent[]> {
    let add: Address = {
      id: 1,
      placeName: "מעון מעין",
      street: "הראה", 
      number: 100,
      city: {
        id: 1,
        name: "רמת גן"
      }
    };

    let tp : VolunteerType = {
      id: 1, 
      name: "קשישים"
    };

    let pr: User = {
      username: "adi",
      firstName: "עדי",
      lastName: "כהן קדוש",
      birthDate: new Date(),
      phone: "0544468995",
      address: {
        id: 2,
        street: "הראה", 
        number: 100,
        city: {
          id: 1,
          name: "רמת גן"
        }
      }
    };

    return of([
      {
          id: 1,
          name: "התנדבות במעון יום לקשישים",
          description: "התנדבות במעון יום לקשישים על שם מעין מאיר. ההתנדבות כוללת לשמח אותם, לשמוע אותם מדברים, להאכיל אותם",
          address: add,
          date: new Date(),
          type: tp,
          promoter: pr
      },
      {
          id: 2,
          name: "התנדבות במעון לילה לקשישים",
          description: "להשכיב אותם לישון",
          address: add,
          date: new Date(),
          type: tp,
          promoter: pr
      },{
        id: 3,
        name: "הוצאת כלבים לטיול בצער בעלי חיים",
        description: "גולדן באזר!!!!!",
        address: add,
        date: new Date(),
        type: {
          id: 2, 
          name: "חיות"
        },
        promoter: pr
    },
    {
        id: 4,
        name: "מיון ובדיקה של תרופות",
        description: "לחלק תרופות",
        address: add,
        date: new Date(),
        type: tp,
        promoter: pr
    },
    {
      id: 5,
      name: "אירוח תושבים בבית",
      description: "ההתנדבות כוללת לשמח אותם, לשמוע אותם מדברים, להאכיל אותם",
      address: add,
      date: new Date(),
      type: tp,
      promoter: pr
  },
  {
      id: 6,
      name: "שיחות טלפון לקשישים",
      description: "לוודא שהם בסדר",
      address: add,
      date: new Date(),
      type: tp,
      promoter: pr
  },
  {
    id: 7,
    name: "חלוקת מזון",
    description: "אוכללל קדימה אוכלללל",
    address: add,
    date: new Date(),
    type: tp,
    promoter: pr
},
{
    id: 8,
    name: "התנדבות במעון לילה לקשישים",
    description: "להשכיב אותם לישון",
    address: add,
    date: new Date(),
    type: tp,
    promoter: pr
}
    ]);
  }

  constructor() { }
}
